package id.co.indivara.jdt12.warehousing;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.indivara.jdt12.warehousing.entity.Merchandise;
import id.co.indivara.jdt12.warehousing.repo.MerchandiseRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class MerchandiseControllerTest {

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    MerchandiseRepository merchandiseRepository;

    @Test
    public void merchandiseCreateTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        Merchandise merchandise = new Merchandise();
        merchandise.setMerchandiseName("Merchandise Name");

        mockMvc.perform(
                        post("/merchandise/create")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(merchandise))
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.merchandiseId").exists())
                .andExpect(jsonPath("$.merchandiseCode").exists())
                .andExpect(jsonPath("$.merchandiseName").value("Merchandise Name"));
    }

    @Test
    public void merchandiseCreateFailedUnauthorizedTest() throws Exception {


        Merchandise merchandise = new Merchandise();
        merchandise.setMerchandiseName("Merchandise Name");

        mockMvc.perform(
                        post("/merchandise/create")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(merchandise)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void merchandiseCreateFailedNotFoundTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        Merchandise merchandise = new Merchandise();
        merchandise.setMerchandiseName("Merchandise Name");

        mockMvc.perform(
                        post("/merchandise/createe")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(merchandise))
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isNotFound());
    }

    @Test
    public void merchandiseCreateFailedBadRequestTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        mockMvc.perform(
                        post("/merchandise/create")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void merchandiseFindTest() throws Exception {
        mockMvc.perform(
                        get("/merchandise/find/{merchandiseId}",3L)
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.merchandiseId").exists())
                .andExpect(jsonPath("$.merchandiseCode").exists())
                .andExpect(jsonPath("$.merchandiseName").value("Merchandise Name"));
    }
}
