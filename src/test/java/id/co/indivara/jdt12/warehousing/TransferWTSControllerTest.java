package id.co.indivara.jdt12.warehousing;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.indivara.jdt12.warehousing.entity.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TransferWTSControllerTest {
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void transferWTSSuccessTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        Warehouse warehouse = new Warehouse();
        warehouse.setWarehouseId(1L);

        Merchandise merchandise = new Merchandise();
        merchandise.setMerchandiseId(1L);

        Store store = new Store();
        store.setStoreId(1L);

        TransferWTS transferWTS = new TransferWTS();
        transferWTS.setStock(250);

        mockMvc.perform(
                        post("/transferwts/{warehouseId}/{merchandiseId}/{storeId}",warehouse.getWarehouseId(),merchandise.getMerchandiseId(),store.getStoreId())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(merchandise.getMerchandiseId()))
                                .content(objectMapper.writeValueAsString(warehouse.getWarehouseId()))
                                .content(objectMapper.writeValueAsString(store.getStoreId()))
                                .content(objectMapper.writeValueAsString(transferWTS))
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.transferWTSId").exists())
                .andExpect(jsonPath("$.transferWTSCode").exists())
                .andExpect(jsonPath("$.source.warehouseId").value(1L)) //yang ngirim
                .andExpect(jsonPath("$.merchandiseId.merchandiseId").value(1L))
                .andExpect(jsonPath("$.destination.storeId").value(1L)) //yang nerima
                .andExpect(jsonPath("$.stock").value(250)) //jumlah yang di transfer
                .andExpect(jsonPath("$.timestamp").exists());

        mockMvc.perform(get("/view/warehouseinventory/warehouse/{warehouseId}", warehouse.getWarehouseId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isOk());
               // .andExpect(jsonPath("$.[0]stock").value(250)); //expect di tempat yg ngirim bakal berkurang jadi berapa

        mockMvc.perform(get("/view/storeinventory/store/{storeId}", store.getStoreId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization","Basic " + readerKey))
                .andExpect(status().isOk());
               // .andExpect(jsonPath("$.[0]stock").value(250)); //expect di tempat yg nerima bakal nambah jadi berapa

    }
    @Test
    public void transferWTSFailedInternalServerErrorTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        Warehouse warehouse = new Warehouse();
        warehouse.setWarehouseId(1000L);

        Merchandise merchandise = new Merchandise();
        merchandise.setMerchandiseId(1L);

        Store store = new Store();
        store.setStoreId(1L);

        TransferWTS transferWTS = new TransferWTS();
        transferWTS.setStock(250);

        mockMvc.perform(
                        post("/transferwts/{warehouseId}/{merchandiseId}/{storeId}",warehouse.getWarehouseId(),merchandise.getMerchandiseId(),store.getStoreId())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(merchandise.getMerchandiseId()))
                                .content(objectMapper.writeValueAsString(warehouse.getWarehouseId()))
                                .content(objectMapper.writeValueAsString(store.getStoreId()))
                                .content(objectMapper.writeValueAsString(transferWTS))
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isInternalServerError());
    }
}
