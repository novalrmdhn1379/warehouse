package id.co.indivara.jdt12.warehousing;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.indivara.jdt12.warehousing.entity.Merchandise;
import id.co.indivara.jdt12.warehousing.entity.TransferWTW;
import id.co.indivara.jdt12.warehousing.entity.Warehouse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TransferWTWControllerTest {
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void transferWTWTSuccessTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        //inputan buat yang ngirim
        Warehouse warehouse = new Warehouse();
        warehouse.setWarehouseId(1L);

        //inputan buat yg nerima
        Warehouse warehouse1 = new Warehouse();
        warehouse1.setWarehouseId(2L);

        Merchandise merchandise = new Merchandise();
        merchandise.setMerchandiseId(1L);

        TransferWTW transferWTW = new TransferWTW();
        transferWTW.setStock(500);

        mockMvc.perform(
                        post("/transferwtw/{warehouseSourceId}/{merchandiseId}/{warehouseDestinationId}",warehouse.getWarehouseId(),merchandise.getMerchandiseId(),warehouse1.getWarehouseId())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(warehouse.getWarehouseId()))
                                .content(objectMapper.writeValueAsString(merchandise.getMerchandiseId()))
                                .content(objectMapper.writeValueAsString(warehouse1.getWarehouseId()))
                                .content(objectMapper.writeValueAsString(transferWTW))
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.transferWTWId").exists())
                .andExpect(jsonPath("$.transferWTWCode").exists())
                .andExpect(jsonPath("$.warehouseSource.warehouseId").value(1L)) //yg ngirim
                .andExpect(jsonPath("$.merchandiseId.merchandiseId").value(1L))
                .andExpect(jsonPath("$.warehouseDestination.warehouseId").value(2L))//yg nerima
                .andExpect(jsonPath("$.stock").value(500)) //jumlah transfer
                .andExpect(jsonPath("$.timestamp").exists());

        mockMvc.perform(get("/view/warehouseinventory/warehouse/{warehouseId}", warehouse.getWarehouseId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization","Basic " + readerKey))
                .andExpect(status().isOk());
               // .andExpect(jsonPath("$.[0]stock").value(500)); //expect di tempat yg ngirim bakal berkurang jadi berapa

        mockMvc.perform(get("/view/warehouseinventory/warehouse/{warehouseId}", warehouse1.getWarehouseId())
                        .accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Authorization","Basic " + readerKey))
                .andExpect(status().isOk());
                //.andExpect(jsonPath("$.[0]stock").value(500)); //expect di tempat yg nerima bakal nambah jadi berapa
    }

    @Test
    public void transferWTWTFailedInternalServerErrorTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        //inputan buat yang ngirim
        Warehouse warehouse = new Warehouse();
        warehouse.setWarehouseId(10000L);

        //inputan buat yg nerima
        Warehouse warehouse1 = new Warehouse();
        warehouse1.setWarehouseId(2L);

        Merchandise merchandise = new Merchandise();
        merchandise.setMerchandiseId(1L);

        TransferWTW transferWTW = new TransferWTW();
        transferWTW.setStock(500);

        mockMvc.perform(
                        post("/transferwtw/{warehouseSourceId}/{merchandiseId}/{warehouseDestinationId}",warehouse.getWarehouseId(),merchandise.getMerchandiseId(),warehouse1.getWarehouseId())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(warehouse.getWarehouseId()))
                                .content(objectMapper.writeValueAsString(merchandise.getMerchandiseId()))
                                .content(objectMapper.writeValueAsString(warehouse1.getWarehouseId()))
                                .content(objectMapper.writeValueAsString(transferWTW))
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isInternalServerError());
    }
}
