package id.co.indivara.jdt12.warehousing;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.indivara.jdt12.warehousing.entity.Store;
import id.co.indivara.jdt12.warehousing.repo.StoreRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class StoreControllerTest {
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    StoreRepository storeRepository;

    @Test
    public void storeCreateFailedUnauthorizedTest() throws Exception {

        Store store = new Store();
        store.setStoreName("Store Name");
        store.setStoreLocation("Place");

        mockMvc.perform(
                        post("/store/create")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(store)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void storeCreateFailedNotFoundTest() throws Exception {

        Store store = new Store();
        store.setStoreName("Store Name");
        store.setStoreLocation("Place");

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        mockMvc.perform(
                        post("/store/createe")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(store))
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isNotFound());
    }

    @Test
    public void storeCreateFailedBadRequestTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        mockMvc.perform(
                        post("/store/create")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void storeCreateTest() throws Exception {

        Store store = new Store();
        store.setStoreName("Store Place");
        store.setStoreLocation("Place");

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        mockMvc.perform(
                        post("/store/create")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(store))
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.storeId").exists())
                .andExpect(jsonPath("$.storeCode").exists())
                .andExpect(jsonPath("$.storeName").value("Store Place"))
                .andExpect(jsonPath("$.storeLocation").value("Place"))
                .andExpect(jsonPath("$.createdDate").exists());
    }

    @Test
    public void storeFindTest() throws Exception {

        mockMvc.perform(
                        get("/store/find/{storeId}",4L)
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.storeId").exists())
                .andExpect(jsonPath("$.storeCode").exists())
                .andExpect(jsonPath("$.storeName").value("Store Name"))
                .andExpect(jsonPath("$.storeLocation").value("Place"))
                .andExpect(jsonPath("$.createdDate").exists());
    }

}
