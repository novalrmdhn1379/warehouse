package id.co.indivara.jdt12.warehousing;
import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.indivara.jdt12.warehousing.entity.Merchandise;
import id.co.indivara.jdt12.warehousing.entity.TransferSupply;
import id.co.indivara.jdt12.warehousing.entity.Warehouse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class TransferSupplyControllerTest {
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void transferSupplySuccessTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        Warehouse warehouse = new Warehouse();
        warehouse.setWarehouseId(1L);

        Merchandise merchandise = new Merchandise();
        merchandise.setMerchandiseId(1L);

        TransferSupply transferSupply = new TransferSupply();
        transferSupply.setStock(1000);

        mockMvc.perform(
                        post("/transfer/{warehouseId}/{merchandiseId}",warehouse.getWarehouseId(),merchandise.getMerchandiseId())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(warehouse.getWarehouseId()))
                                .content(objectMapper.writeValueAsString(merchandise.getMerchandiseId()))
                                .content(objectMapper.writeValueAsString(transferSupply))
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.trxSupplyId").exists())
                .andExpect(jsonPath("$.trxSupplyCode").exists())
                .andExpect(jsonPath("$.warehouse.warehouseCode").exists())
                .andExpect(jsonPath("$.merchandise.merchandiseCode").exists())
                .andExpect(jsonPath("$.stock").value(1000))
                .andExpect(jsonPath("$.timestamp").exists());
    }

    @Test
    public void transferSupplyFailedInternalServerErrorTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        Warehouse warehouse = new Warehouse();
        warehouse.setWarehouseId(1000L);

        Merchandise merchandise = new Merchandise();
        merchandise.setMerchandiseId(2L);

        TransferSupply transferSupply = new TransferSupply();
        transferSupply.setStock(1000);

        mockMvc.perform(
                        post("/transfer/{warehouseId}/{merchandiseId}",warehouse.getWarehouseId(),merchandise.getMerchandiseId())
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(warehouse.getWarehouseId()))
                                .content(objectMapper.writeValueAsString(merchandise.getMerchandiseId()))
                                .content(objectMapper.writeValueAsString(transferSupply))
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isInternalServerError());
    }
}
