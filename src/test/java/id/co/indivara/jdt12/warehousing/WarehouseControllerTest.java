package id.co.indivara.jdt12.warehousing;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.co.indivara.jdt12.warehousing.entity.Warehouse;
import id.co.indivara.jdt12.warehousing.repo.WarehouseRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class WarehouseControllerTest {
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    WarehouseRepository warehouseRepository;

    @Test
    public void warehouseCreateTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        Warehouse warehouse = new Warehouse();
        warehouse.setWarehouseName("Warehouse Name");
        warehouse.setWarehouseLocation("Place");

        mockMvc.perform(
                        post("/warehouse/create")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(warehouse))
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.warehouseId").exists())
                .andExpect(jsonPath("$.warehouseCode").exists())
                .andExpect(jsonPath("$.warehouseName").value("Warehouse Name"))
                .andExpect(jsonPath("$.warehouseLocation").value("Place"))
                .andExpect(jsonPath("$.createdDate").exists());
    }

    @Test
    public void warehouseCreateFailedUnauthorizedTest() throws Exception {
        Warehouse warehouse = new Warehouse();
        warehouse.setWarehouseName("Warehouse Name");
        warehouse.setWarehouseLocation("Place");

        mockMvc.perform(
                        post("/warehouse/create")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(warehouse)))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void warehouseCreateFailedNotFoundTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";

        Warehouse warehouse = new Warehouse();
        warehouse.setWarehouseName("Warehouse Name");
        warehouse.setWarehouseLocation("Place");

        mockMvc.perform(
                        post("/create/warehousee")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(warehouse))
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isNotFound());
    }

    @Test
    public void warehouseCreateFailedBadRequestTest() throws Exception {

        String readerKey = "YWRtaW46cGFzc3dvcmQ=";


        mockMvc.perform(
                        post("/warehouse/create")
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON)
                                .header("Authorization","Basic " + readerKey))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void warehouseFindTest() throws Exception {
        mockMvc.perform(
                        get("/warehouse/find/{warehouseId}",4L)
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.warehouseId").exists())
                .andExpect(jsonPath("$.warehouseCode").exists())
                .andExpect(jsonPath("$.warehouseName").value("Warehouse Name"))
                .andExpect(jsonPath("$.warehouseLocation").value("Place"))
                .andExpect(jsonPath("$.createdDate").exists());
    }
}
