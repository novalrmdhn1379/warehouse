package id.co.indivara.jdt12.warehousing.service;

import id.co.indivara.jdt12.warehousing.entity.TransferSupply;
import id.co.indivara.jdt12.warehousing.entity.TransferWTS;
import id.co.indivara.jdt12.warehousing.entity.TransferWTW;
import id.co.indivara.jdt12.warehousing.entity.Warehouse;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public interface TransactionService {
    List<TransferSupply> getTransferSupply(Warehouse warehouseId);
    List<TransferWTW> getTransferWTW(Warehouse warehouseId);
    List<TransferWTS> getTransferWTS(Warehouse warehouseId);
    Map<String, Object> findTransaction(Warehouse warehouseId);
    List<TransferSupply> findTransferSupply(Warehouse warehouseId);
    List<TransferWTW> findTransferWTW(Warehouse warehouseId);
    List<TransferWTS> findTransferWTS(Warehouse warehouseId);
}
