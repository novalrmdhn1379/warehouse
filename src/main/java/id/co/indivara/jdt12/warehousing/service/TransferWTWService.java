package id.co.indivara.jdt12.warehousing.service;

import id.co.indivara.jdt12.warehousing.entity.Merchandise;
import id.co.indivara.jdt12.warehousing.entity.TransferWTW;
import id.co.indivara.jdt12.warehousing.entity.Warehouse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TransferWTWService {

    TransferWTW transferWTW(Warehouse warehouseSourceId, Merchandise merchandiseId,Warehouse warehouseDestinationId,TransferWTW transferWTW);
    List<TransferWTW> findAll();

}
