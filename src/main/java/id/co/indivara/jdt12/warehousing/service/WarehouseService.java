package id.co.indivara.jdt12.warehousing.service;

import id.co.indivara.jdt12.warehousing.entity.Warehouse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface WarehouseService {
    Warehouse createWarehouse(Warehouse warehouse);
    Warehouse findById(Long warehouseId);
    List<Warehouse> findAll();
    void deleteWarehouse(Long warehouseId);

}
