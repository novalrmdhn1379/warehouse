package id.co.indivara.jdt12.warehousing.service;

import id.co.indivara.jdt12.warehousing.entity.Merchandise;
import id.co.indivara.jdt12.warehousing.entity.Store;
import id.co.indivara.jdt12.warehousing.entity.TransferWTS;
import id.co.indivara.jdt12.warehousing.entity.Warehouse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TransferWTSService {

    TransferWTS transferWTS(Warehouse warehouseId, Merchandise merchandiseId, Store storeId,TransferWTS transferWTS);
    List<TransferWTS> findAll();

}
