package id.co.indivara.jdt12.warehousing.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "store")
public class Store {

    @Id
    @Column(name = "store_id",nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long storeId;

    @Column(name = "store_code",nullable = false, length = 32)
    private String storeCode;

    @Column(name = "store_name",nullable = false,length = 50)
    private String storeName;

    @Column(name = "store_location",nullable = false,length = 50)
    private String storeLocation;

    @Column(name = "created_date",nullable = false)
    private Timestamp createdDate;
}
