package id.co.indivara.jdt12.warehousing.serviceImpl;

import id.co.indivara.jdt12.warehousing.entity.Merchandise;
import id.co.indivara.jdt12.warehousing.entity.Warehouse;
import id.co.indivara.jdt12.warehousing.entity.WarehouseInventory;
import id.co.indivara.jdt12.warehousing.repo.WarehouseInventoryRepository;
import id.co.indivara.jdt12.warehousing.service.WarehouseInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WarehouseInventoryServiceImpl implements WarehouseInventoryService {

    @Autowired
    WarehouseInventoryRepository warehouseInventoryRepository;
    @Override
    public List<WarehouseInventory> findAll() {
        return warehouseInventoryRepository.findAll();
    }

    @Override
    public List<WarehouseInventory> findByWarehouseId(Warehouse warehouseId) {
        return warehouseInventoryRepository.findByWarehouseId(warehouseId);
    }

    @Override
    public List<WarehouseInventory> findByMerchandiseId(Merchandise merchandiseId) {
        return warehouseInventoryRepository.findByMerchandiseId(merchandiseId);
    }
}
