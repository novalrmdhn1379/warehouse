package id.co.indivara.jdt12.warehousing.controller;

import id.co.indivara.jdt12.warehousing.entity.Merchandise;
import id.co.indivara.jdt12.warehousing.entity.Warehouse;
import id.co.indivara.jdt12.warehousing.entity.WarehouseInventory;
import id.co.indivara.jdt12.warehousing.repo.WarehouseInventoryRepository;
import id.co.indivara.jdt12.warehousing.service.WarehouseInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class WarehouseInventoryController {

    @Autowired
    WarehouseInventoryService warehouseInventoryService;

    @GetMapping("/view/warehouse/all")
    @PreAuthorize("hasRole('ADMIN') or hasRole('WAREHOUSEADMIN')")
    public List<WarehouseInventory> viewAllWarehouseInventory(){
        return warehouseInventoryService.findAll();
    }

    @GetMapping("/view/warehouseinventory/warehouse/{warehouseId}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('WAREHOUSEADMIN')")
    public List<WarehouseInventory> viewWarehouseInventory(@PathVariable Warehouse warehouseId){
        return warehouseInventoryService.findByWarehouseId(warehouseId);
    }

    @GetMapping("/view/warehouseinventory/merchandise/{merchandiseId}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('WAREHOUSEADMIN')")
    public List<WarehouseInventory> viewWarehouseInventory(@PathVariable Merchandise merchandiseId){
        return warehouseInventoryService.findByMerchandiseId(merchandiseId);
    }

}
