package id.co.indivara.jdt12.warehousing.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "merchandises")
public class Merchandise {

    @Id
    @Column(name = "merchandise_id",nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long merchandiseId;

    @Column(name = "merchandise_code",nullable = false, length = 32)
    private String merchandiseCode;

    @Column(name = "merchandise_name",nullable = false,length = 50)
    private String merchandiseName;
}
