package id.co.indivara.jdt12.warehousing.controller;

import id.co.indivara.jdt12.warehousing.entity.Merchandise;
import id.co.indivara.jdt12.warehousing.entity.Store;
import id.co.indivara.jdt12.warehousing.entity.StoreInventory;
import id.co.indivara.jdt12.warehousing.repo.StoreInventoryRepository;
import id.co.indivara.jdt12.warehousing.service.StoreInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StoreInventoryController {

    @Autowired
    StoreInventoryService storeInventoryService;

    @GetMapping("/view/store/all")
    @PreAuthorize("hasRole('ADMIN') or hasRole('STOREADMIN')")
    public List<StoreInventory> viewAllStoreInventory(){
       return storeInventoryService.findAll();
    }

    @GetMapping("/view/storeinventory/store/{storeId}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('STOREADMIN')")
    public List<StoreInventory> viewStoreInventory(@PathVariable Store storeId){
       return storeInventoryService.findByStoreId(storeId);
    }

    @GetMapping("/view/storeinventory/merchandise/{merchandiseId}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('STOREADMIN')")
    public List<StoreInventory> viewStoreInventory(@PathVariable Merchandise merchandiseId){
        return storeInventoryService.findByMerchandiseId(merchandiseId);
    }

}
