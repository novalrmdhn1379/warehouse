package id.co.indivara.jdt12.warehousing.controller;

import id.co.indivara.jdt12.warehousing.entity.Warehouse;
import id.co.indivara.jdt12.warehousing.repo.WarehouseRepository;
import id.co.indivara.jdt12.warehousing.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/warehouse")
public class WarehouseController {

    @Autowired
    WarehouseService warehouseService;

    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    public Warehouse createWarehouse(@RequestBody Warehouse warehouse){
        return warehouseService.createWarehouse(warehouse);
    }

    @GetMapping("/find/{warehouseId}")
    public Warehouse findById(@PathVariable Long warehouseId){
        return warehouseService.findById(warehouseId);
    }

    @GetMapping("/find/all")
    public List<Warehouse> findAll(){
        return warehouseService.findAll();
    }


    @DeleteMapping("/delete/{warehouseId}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteWarehouse(@PathVariable Long warehouseId){
        warehouseService.deleteWarehouse(warehouseId);
    }
}
