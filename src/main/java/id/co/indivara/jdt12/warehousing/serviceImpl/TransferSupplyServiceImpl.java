package id.co.indivara.jdt12.warehousing.serviceImpl;

import id.co.indivara.jdt12.warehousing.entity.*;
import id.co.indivara.jdt12.warehousing.repo.TransactionRepository;
import id.co.indivara.jdt12.warehousing.repo.TransferSupplyRepository;
import id.co.indivara.jdt12.warehousing.repo.WarehouseInventoryRepository;
import id.co.indivara.jdt12.warehousing.service.TransferSupplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class TransferSupplyServiceImpl implements TransferSupplyService {
    @Autowired
    TransferSupplyRepository transferSupplyRepository;
    @Autowired
    TransactionRepository transactionRepository;
    @Autowired
    WarehouseInventoryRepository warehouseInventoryRepository;

    @Override
    public TransferSupply transferSupply(Warehouse warehouseId, Merchandise merchandiseId, TransferSupply transferSupply) {
        //buat cek warehousenya ada dan punya merchandise atau tidak
        WarehouseInventory warehouseInventory = warehouseInventoryRepository.findByMerchandiseIdAndWarehouseId(merchandiseId,warehouseId);

        //ngeset kedalam tabel transfersupply
        transferSupply.setTrxSupplyCode("T" + (transferSupplyRepository.count()+1));
        transferSupply.setMerchandise(merchandiseId);
        transferSupply.setWarehouse(warehouseId);
        transferSupply.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
        transferSupplyRepository.save(transferSupply);

        //ngeset kedalam tabel transaction
        Transaction transaction = new Transaction();
        transaction.setTransactionCode("trx"+(transactionRepository.count()+1));
        transaction.setType("trx_supply");
        transaction.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
        transactionRepository.save(transaction);

        //object baru buat warehouse inventory kalo ternyata cek diatas gagal
        WarehouseInventory warehouseInventory1 = new WarehouseInventory();

       if (warehouseInventory != null) {
           warehouseInventory.setStock(warehouseInventory.getStock() + transferSupply.getStock());
           warehouseInventoryRepository.save(warehouseInventory);
       } else if (warehouseInventory == null) {
        warehouseInventory1.setMerchandiseId(merchandiseId);
            warehouseInventory1.setWarehouseId(warehouseId);
            warehouseInventory1.setStock(transferSupply.getStock());
            warehouseInventoryRepository.save(warehouseInventory1);
        }
        return transferSupply;
    }

    @Override
    public List<TransferSupply> findAll() {
        return transferSupplyRepository.findAll();
    }
}
