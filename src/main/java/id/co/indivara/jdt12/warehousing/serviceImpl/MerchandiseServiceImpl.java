package id.co.indivara.jdt12.warehousing.serviceImpl;

import id.co.indivara.jdt12.warehousing.entity.Merchandise;
import id.co.indivara.jdt12.warehousing.repo.MerchandiseRepository;
import id.co.indivara.jdt12.warehousing.service.MerchandiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class MerchandiseServiceImpl implements MerchandiseService {

    @Autowired
    private MerchandiseRepository merchandiseRepository;

    @Override
    public Merchandise createMerchandise(Merchandise merchandise) {
        Merchandise ms = new Merchandise();
        ms.setMerchandiseCode("mrc" + (merchandiseRepository.count() + 1));
        ms.setMerchandiseName(merchandise.getMerchandiseName());
        return merchandiseRepository.save(ms);
    }

    @Override
    public Merchandise findById(Long merchandiseId) {
        return merchandiseRepository.findById(merchandiseId).get();
    }

    @Override
    public List<Merchandise> findAll() {
        return merchandiseRepository.findAll();
    }

    @Override
    public void deleteMerchandise(Long merchandiseId) {
        merchandiseRepository.deleteById(merchandiseId);
    }
}
