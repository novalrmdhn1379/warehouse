package id.co.indivara.jdt12.warehousing.service;

import id.co.indivara.jdt12.warehousing.entity.Merchandise;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface MerchandiseService {
    Merchandise createMerchandise(Merchandise merchandise);
    Merchandise findById(Long merchandiseId);
    List<Merchandise> findAll();
    void deleteMerchandise(Long merchandiseId);
}
