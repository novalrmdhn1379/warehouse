package id.co.indivara.jdt12.warehousing.repo;

import id.co.indivara.jdt12.warehousing.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StoreInventoryRepository extends JpaRepository<StoreInventory,Long> {
    StoreInventory findByMerchandiseIdAndStoreId(Merchandise merchandise, Store store);

    List<StoreInventory> findByStoreId(Store store);
    List<StoreInventory> findByMerchandiseId(Merchandise merchandise);

}
