package id.co.indivara.jdt12.warehousing.service;

import id.co.indivara.jdt12.warehousing.entity.Merchandise;
import id.co.indivara.jdt12.warehousing.entity.TransferSupply;
import id.co.indivara.jdt12.warehousing.entity.Warehouse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface TransferSupplyService {

    TransferSupply transferSupply(Warehouse warehouseId, Merchandise merchandiseId,TransferSupply transferSupply);
    List<TransferSupply> findAll();
}
