package id.co.indivara.jdt12.warehousing.serviceImpl;

import id.co.indivara.jdt12.warehousing.entity.Store;
import id.co.indivara.jdt12.warehousing.repo.StoreRepository;
import id.co.indivara.jdt12.warehousing.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class StoreServiceImpl implements StoreService {

    @Autowired
    StoreRepository storeRepository;

    @Override
    public Store createStore(Store store) {
        Store str = new Store();
        str.setStoreCode("str"+(storeRepository.count()+1));
        str.setStoreName(store.getStoreName());
        str.setStoreLocation(store.getStoreLocation());
        str.setCreatedDate(Timestamp.valueOf(LocalDateTime.now()));
        return storeRepository.save(str);
    }

    @Override
    public Store findById(Long storeId) {
        return storeRepository.findById(storeId).get();
    }

    @Override
    public List<Store> findAll() {
        return storeRepository.findAll();
    }

    @Override
    public void deleteStore(Long storeId) {
        storeRepository.deleteById(storeId);
    }
}
