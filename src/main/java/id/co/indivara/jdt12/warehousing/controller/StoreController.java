package id.co.indivara.jdt12.warehousing.controller;

import id.co.indivara.jdt12.warehousing.entity.Store;
import id.co.indivara.jdt12.warehousing.repo.StoreRepository;
import id.co.indivara.jdt12.warehousing.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/store")
public class StoreController {

    @Autowired
    StoreService storeService;

    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    public Store createStore(@RequestBody Store store){
       return storeService.createStore(store);
    }

    @GetMapping("/find/{storeId}")
    public Store findById(@PathVariable Long storeId){
        return storeService.findById(storeId);
    }

    @GetMapping("/find/all")
    public List<Store> findAll(){
        return storeService.findAll();
    }

    @DeleteMapping("/delete/{storeId}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteStore(@PathVariable Long storeId){
     storeService.deleteStore(storeId);
    }
}
