package id.co.indivara.jdt12.warehousing.serviceImpl;

import id.co.indivara.jdt12.warehousing.entity.*;
import id.co.indivara.jdt12.warehousing.repo.TransactionRepository;
import id.co.indivara.jdt12.warehousing.repo.TransferWTWRepository;
import id.co.indivara.jdt12.warehousing.repo.WarehouseInventoryRepository;
import id.co.indivara.jdt12.warehousing.service.TransferWTWService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Service
public class TransferWTWServiceImpl implements TransferWTWService {
    @Autowired
    WarehouseInventoryRepository warehouseInventoryRepository;
    @Autowired
    TransferWTWRepository transferWTWRepository;
    @Autowired
    TransactionRepository transactionRepository;
    @Override
    public TransferWTW transferWTW(Warehouse warehouseSourceId, Merchandise merchandiseId, Warehouse warehouseDestinationId,TransferWTW transferWTW) {
        WarehouseInventory warehouseSource = warehouseInventoryRepository.findByMerchandiseIdAndWarehouseId(merchandiseId,warehouseSourceId);
        WarehouseInventory warehouseDestination = warehouseInventoryRepository.findByMerchandiseIdAndWarehouseId(merchandiseId,warehouseDestinationId);

        //ngeset kedalam tabel transferwtw
        transferWTW.setTransferWTWCode("T" + (transferWTWRepository.count()+1));
        transferWTW.setWarehouseSource(warehouseSourceId);
        transferWTW.setWarehouseDestination(warehouseDestinationId);
        transferWTW.setMerchandiseId(merchandiseId);
        transferWTW.setStock(transferWTW.getStock());
        transferWTW.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
        transferWTWRepository.save(transferWTW);

        //ngeset kedalam tabel transaction
        Transaction transaction = new Transaction();
        transaction.setTransactionCode("trx"+(transactionRepository.count()+1));
        transaction.setType("trx_wtw");
        transaction.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
        transactionRepository.save(transaction);

        //object baru buat warehouse inventory kalo ternyata gak punya merchandise
        WarehouseInventory warehouseInventory = new WarehouseInventory();

        if (warehouseSource.getStock()>= transferWTW.getStock()){
            warehouseSource.setStock(warehouseSource.getStock()-transferWTW.getStock());
        }

        if (warehouseDestination != null){
            warehouseDestination.setStock(warehouseDestination.getStock()+ transferWTW.getStock());
            warehouseInventoryRepository.save(warehouseDestination);
        } else if (warehouseDestination == null) {
            warehouseInventory = new WarehouseInventory();
            warehouseInventory.setWarehouseId(warehouseDestinationId);
            warehouseInventory.setMerchandiseId(merchandiseId);
            warehouseInventory.setStock(transferWTW.getStock());
            warehouseInventoryRepository.save(warehouseInventory);
        }
        return new ResponseEntity<>(transferWTW,HttpStatus.OK).getBody();
    }

    @Override
    public List<TransferWTW> findAll() {
        return transferWTWRepository.findAll();
    }
}
