package id.co.indivara.jdt12.warehousing.serviceImpl;

import id.co.indivara.jdt12.warehousing.entity.TransferSupply;
import id.co.indivara.jdt12.warehousing.entity.TransferWTS;
import id.co.indivara.jdt12.warehousing.entity.TransferWTW;
import id.co.indivara.jdt12.warehousing.entity.Warehouse;
import id.co.indivara.jdt12.warehousing.repo.TransactionRepository;
import id.co.indivara.jdt12.warehousing.repo.TransferSupplyRepository;
import id.co.indivara.jdt12.warehousing.repo.TransferWTSRepository;
import id.co.indivara.jdt12.warehousing.repo.TransferWTWRepository;
import id.co.indivara.jdt12.warehousing.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TransactionServiceImpl implements TransactionService {
    @Autowired
    TransferSupplyRepository transferSupplyRepository;
    @Autowired
    TransferWTSRepository transferWTSRepository;
    @Autowired
    TransferWTWRepository transferWTWRepository;

    @Override
    public List<TransferSupply> getTransferSupply(Warehouse warehouseId) {
        return transferSupplyRepository.findByWarehouse(warehouseId);
    }

    @Override
    public List<TransferWTW> getTransferWTW(Warehouse warehouseId) {
        return transferWTWRepository.findByWarehouseSource(warehouseId);
    }

    @Override
    public List<TransferWTS> getTransferWTS(Warehouse warehouseId) {
        return transferWTSRepository.findBySource(warehouseId);
    }

    @Override
    public Map<String, Object> findTransaction(Warehouse warehouseId) {
        Map combine = new HashMap<List,Object>();
        combine.put("trx_supply",getTransferSupply(warehouseId));
        combine.put("trx_wtw",getTransferWTW(warehouseId));
        combine.put("trx_wts",getTransferWTS(warehouseId));
        return combine;
    }

    @Override
    public List<TransferSupply> findTransferSupply(Warehouse warehouseId) {
        return transferSupplyRepository.findByWarehouse(warehouseId);
    }

    @Override
    public List<TransferWTW> findTransferWTW(Warehouse warehouseId) {
        return transferWTWRepository.findByWarehouseSource(warehouseId);
    }

    @Override
    public List<TransferWTS> findTransferWTS(Warehouse warehouseId) {
        return transferWTSRepository.findBySource(warehouseId);
    }
}
