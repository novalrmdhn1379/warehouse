package id.co.indivara.jdt12.warehousing.serviceImpl;

import id.co.indivara.jdt12.warehousing.entity.*;
import id.co.indivara.jdt12.warehousing.repo.StoreInventoryRepository;
import id.co.indivara.jdt12.warehousing.repo.TransactionRepository;
import id.co.indivara.jdt12.warehousing.repo.TransferWTSRepository;
import id.co.indivara.jdt12.warehousing.repo.WarehouseInventoryRepository;
import id.co.indivara.jdt12.warehousing.service.TransferWTSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Service
public class TransferWTSServiceImpl implements TransferWTSService {

    @Autowired
    StoreInventoryRepository storeInventoryRepository;
    @Autowired
    WarehouseInventoryRepository warehouseInventoryRepository;
    @Autowired
    TransferWTSRepository transferWTSRepository;
    @Autowired
    TransactionRepository transactionRepository;
    @Override
    public TransferWTS transferWTS(Warehouse warehouseId, Merchandise merchandiseId, Store storeId,TransferWTS transferWTS) {
        WarehouseInventory warehouseSource = warehouseInventoryRepository.findByMerchandiseIdAndWarehouseId(merchandiseId,warehouseId);
        StoreInventory storeDestination = storeInventoryRepository.findByMerchandiseIdAndStoreId(merchandiseId,storeId);

        //object baru buat warehouse inventory kalo ternyata gak punya merchandise
        StoreInventory storeInventory = new StoreInventory();

        //ngeset kedalam tabel transferwts
        transferWTS.setTransferWTSCode("T" + (transferWTSRepository.count()+1));
        transferWTS.setMerchandiseId(merchandiseId);
        transferWTS.setSource(warehouseId);
        transferWTS.setDestination(storeId);
        transferWTS.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
        transferWTSRepository.save(transferWTS);

        //ngeset kedalam tabel transaction
        Transaction transaction = new Transaction();
        transaction.setTransactionCode("trx"+(transactionRepository.count()+1));
        transaction.setType("trx_wts");
        transaction.setTimestamp(Timestamp.valueOf(LocalDateTime.now()));
        transactionRepository.save(transaction);

        if (warehouseSource.getStock()>=transferWTS.getStock()){
            warehouseSource.setStock(warehouseSource.getStock()- transferWTS.getStock());
        }

        if (storeDestination != null) {
            storeDestination.setStock(storeDestination.getStock() + transferWTS.getStock());
            storeInventoryRepository.save(storeDestination);
        } else if (storeDestination == null) {
        storeInventory.setStoreId(storeId);
            storeInventory.setMerchandiseId(merchandiseId);
            storeInventory.setStock(transferWTS.getStock());
            storeInventoryRepository.save(storeInventory);
        }
        return new ResponseEntity<>(transferWTS, HttpStatus.OK).getBody();
    }

    @Override
    public List<TransferWTS> findAll() {
        return transferWTSRepository.findAll();
    }
}
