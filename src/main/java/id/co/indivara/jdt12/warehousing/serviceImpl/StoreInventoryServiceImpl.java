package id.co.indivara.jdt12.warehousing.serviceImpl;

import id.co.indivara.jdt12.warehousing.entity.Merchandise;
import id.co.indivara.jdt12.warehousing.entity.Store;
import id.co.indivara.jdt12.warehousing.entity.StoreInventory;
import id.co.indivara.jdt12.warehousing.repo.StoreInventoryRepository;
import id.co.indivara.jdt12.warehousing.service.StoreInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StoreInventoryServiceImpl implements StoreInventoryService {
    @Autowired
    StoreInventoryRepository storeInventoryRepository;
    @Override
    public List<StoreInventory> findAll() {
        return storeInventoryRepository.findAll();
    }

    @Override
    public List<StoreInventory> findByStoreId(Store storeId) {
        return storeInventoryRepository.findByStoreId(storeId);
    }

    @Override
    public List<StoreInventory> findByMerchandiseId(Merchandise merchandiseId) {
        return storeInventoryRepository.findByMerchandiseId(merchandiseId);
    }
}
