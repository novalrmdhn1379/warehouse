package id.co.indivara.jdt12.warehousing.service;

import id.co.indivara.jdt12.warehousing.entity.Store;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StoreService {
    Store createStore(Store store);
    Store findById(Long storeId);
    List<Store> findAll();
    void deleteStore(Long storeId);
}
