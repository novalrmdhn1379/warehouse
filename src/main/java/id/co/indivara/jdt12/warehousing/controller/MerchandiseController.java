package id.co.indivara.jdt12.warehousing.controller;

import id.co.indivara.jdt12.warehousing.entity.Merchandise;
import id.co.indivara.jdt12.warehousing.service.MerchandiseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/merchandise")
public class MerchandiseController {

    @Autowired
    MerchandiseService merchandiseService;

    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMIN')")
    public Merchandise createMerchandise(@RequestBody Merchandise merchandise) {
        return merchandiseService.createMerchandise(merchandise);
    }

    @GetMapping("/find/{merchandiseId}")
    public Merchandise findById(@PathVariable Long merchandiseId) {
        return merchandiseService.findById(merchandiseId);
    }

    @GetMapping("/find/all")
    public List<Merchandise> findAll(){
        return merchandiseService.findAll();
    }

    @DeleteMapping("/delete/{merchandiseId}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteMerchandise(@PathVariable Long merchandiseId) {
        merchandiseService.deleteMerchandise(merchandiseId);
    }
}
