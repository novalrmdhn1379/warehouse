package id.co.indivara.jdt12.warehousing.service;

import id.co.indivara.jdt12.warehousing.entity.Merchandise;
import id.co.indivara.jdt12.warehousing.entity.Store;
import id.co.indivara.jdt12.warehousing.entity.StoreInventory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StoreInventoryService {
    List<StoreInventory> findAll();
    List<StoreInventory> findByStoreId(Store storeId);
    List<StoreInventory> findByMerchandiseId(Merchandise merchandiseId);
}
