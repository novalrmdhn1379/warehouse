package id.co.indivara.jdt12.warehousing.controller;

import id.co.indivara.jdt12.warehousing.entity.*;
import id.co.indivara.jdt12.warehousing.repo.TransactionRepository;
import id.co.indivara.jdt12.warehousing.repo.TransferSupplyRepository;
import id.co.indivara.jdt12.warehousing.repo.WarehouseInventoryRepository;
import id.co.indivara.jdt12.warehousing.service.TransferSupplyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
public class TransferSupplyController {

    @Autowired
    TransferSupplyService transferSupplyService;


    @PostMapping("/transfer/{warehouseId}/{merchandiseId}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('SUPPLYER')")
    public ResponseEntity<TransferSupply> transferSupply(@PathVariable Warehouse warehouseId, @PathVariable Merchandise merchandiseId, @RequestBody TransferSupply transferSupply){
        TransferSupply transferSupply1 = transferSupplyService.transferSupply(warehouseId, merchandiseId, transferSupply);
        return new ResponseEntity<>(transferSupply1,HttpStatus.OK);
    }

    @GetMapping("/find/supply/all")
    public List<TransferSupply> findAllTransferSupply(){
        return transferSupplyService.findAll();
    }
}
