package id.co.indivara.jdt12.warehousing.controller;

import id.co.indivara.jdt12.warehousing.entity.*;
import id.co.indivara.jdt12.warehousing.repo.*;
import id.co.indivara.jdt12.warehousing.service.TransferWTWService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
public class TransferWTWController {

    @Autowired
    TransferWTWService transferWTWService;

    @PostMapping("/transferwtw/{warehouseSourceId}/{merchandiseId}/{warehouseDestinationId}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('WAREHOUSEADMIN')")
    public ResponseEntity<TransferWTW> transferWTW(@PathVariable Warehouse warehouseSourceId, @PathVariable Merchandise merchandiseId, @PathVariable Warehouse warehouseDestinationId, @RequestBody TransferWTW transferwtw) {
      TransferWTW transferWTW = transferWTWService.transferWTW(warehouseSourceId, merchandiseId, warehouseDestinationId, transferwtw);
      return new ResponseEntity<>(transferWTW,HttpStatus.OK);
    }

    @GetMapping("/find/wtw/all")
    public List<TransferWTW> findAllTransferWTW(){
        return transferWTWService.findAll();
    }

}
