package id.co.indivara.jdt12.warehousing.controller;

import id.co.indivara.jdt12.warehousing.entity.*;
import id.co.indivara.jdt12.warehousing.repo.*;
import id.co.indivara.jdt12.warehousing.service.TransferWTSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@RestController
public class TransferWTSController{

    @Autowired
    TransferWTSService transferWTSService;

    @PostMapping("/transferwts/{warehouseCode}/{merchandiseCode}/{storeCode}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('WAREHOUSEADMIN')")
    public ResponseEntity<TransferWTS> transferWTS(@PathVariable Warehouse warehouseCode, @PathVariable Merchandise merchandiseCode, @PathVariable Store storeCode, @RequestBody TransferWTS transferWTS){
      TransferWTS transferWTS1 = transferWTSService.transferWTS(warehouseCode, merchandiseCode, storeCode, transferWTS);
      return new ResponseEntity<>(transferWTS1,HttpStatus.OK);
    }

    @GetMapping("/find/wts/all")
    public List<TransferWTS> findAllTransferWTS(){
        return transferWTSService.findAll();
    }

}
