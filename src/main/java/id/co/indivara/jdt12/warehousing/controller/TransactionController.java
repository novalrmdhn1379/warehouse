package id.co.indivara.jdt12.warehousing.controller;

import id.co.indivara.jdt12.warehousing.entity.TransferSupply;
import id.co.indivara.jdt12.warehousing.entity.TransferWTS;
import id.co.indivara.jdt12.warehousing.entity.TransferWTW;
import id.co.indivara.jdt12.warehousing.entity.Warehouse;
import id.co.indivara.jdt12.warehousing.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class TransactionController {
    @Autowired
    TransactionService transactionService;

    public List<TransferSupply> transferSupply(Warehouse warehouseId){
        return transactionService.getTransferSupply(warehouseId);
    }

    public List<TransferWTW> transferWTW(Warehouse warehouseId){
        return transactionService.getTransferWTW(warehouseId);
    }

    public List<TransferWTS> transferWTS(Warehouse warehouseId){
        return transactionService.getTransferWTS(warehouseId);
    }

    @GetMapping("/find/transaction/{warehouseId}")
    @PreAuthorize("hasRole('ADMIN')")
    public Map<String, Object> findTransaction(@PathVariable("warehouseId") Warehouse warehouseId){
        return transactionService.findTransaction(warehouseId);
    }

    @GetMapping("/find/transaction/supply/{warehouseId}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('WAREHOUSEADMIN')")
    public List<TransferSupply> findTransactionSupply(@PathVariable Warehouse warehouseId){
        return transactionService.findTransferSupply(warehouseId);
    }

    @GetMapping("/find/transaction/wtw/{warehouseId}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('WAREHOUSEADMIN')")
    public List<TransferWTW> findTransactionWTW(@PathVariable Warehouse warehouseId){
        return transactionService.findTransferWTW(warehouseId);
    }

    @GetMapping("/find/transaction/wts/{warehouseId}")
    @PreAuthorize("hasRole('ADMIN') or hasRole('WAREHOUSEADMIN') or hasRole('STOREADMIN')")
    public List<TransferWTS> findTransactionWTS(@PathVariable Warehouse warehouseId){
        return transactionService.findTransferWTS(warehouseId);
    }
}
