package id.co.indivara.jdt12.warehousing.service;

import id.co.indivara.jdt12.warehousing.entity.Merchandise;
import id.co.indivara.jdt12.warehousing.entity.Warehouse;
import id.co.indivara.jdt12.warehousing.entity.WarehouseInventory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface WarehouseInventoryService {
    List<WarehouseInventory> findAll();
    List<WarehouseInventory> findByWarehouseId(Warehouse warehouseId);
    List<WarehouseInventory> findByMerchandiseId(Merchandise merchandiseId);
}
